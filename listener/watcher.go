package listener

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
)

var (
	subscribeMessages = []string{
		"subscribe 1",
		"subscribe 2",
		"subscribe 3",
	}
	settingMessages = []string{
		"setting 1",
		"setting 2",
		"setting 3",
	}
)

func Watch(bot *tgbotapi.BotAPI) {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		log.Panicln(err)
	}

	for update := range updates {
		if update.Message == nil {
			//ignore any non-message updates
			continue
		}

		if update.CallbackQuery == nil {
			continue
		}

		log.Println("Message received: %s, ChatId: %s", update.Message.Text, update.Message.Chat.ID)
		handleIncomingMessage(update.Message)
	}
}

func handleIncomingMessage(msg *tgbotapi.Message) {
	switch true {
	case confirmMsg(subscribeMessages, msg.Text):
		// send message to one service
		return
	case confirmMsg(settingMessages, msg.Text):
		//send to another
		return
	default:
		//default handling
	}
}

func confirmMsg(list []string, msg string) bool {
	for _, v := range list {
		if v == msg {
			return true
		}
	}
	return false
}
