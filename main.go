package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/germes/client/listener"
	"log"
	"os"
)


func main() {
	log.Println("Service client started")
	bot := getBotConnection()
	go listener.Watch(bot)

	select{}
}

func getBotConnection() *tgbotapi.BotAPI {
	token, exist := os.LookupEnv("TELEGRAM_BOT_API_TOKEN")
	if !exist {
		log.Panicln("No telegram API token found")
	}
	log.Printf("Starting with token %s\n", token)

	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panicln(err)
	}

	log.Printf("Authorizen with account %s\n", bot.Self.UserName)

	bot.Debug = true

	return bot
}


