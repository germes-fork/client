FROM golang:1.17-alpine3.13 as builder

WORKDIR /go/src/gitlab.com/germes/client
COPY go.mod /go/src/gitlab.com/germes/client
RUN go mod download
COPY . /go/src/gitlab.com/germes/client
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/client gitlab.com/germes/client

FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates
COPY --from=builder /go/src/gitlab.com/germes/client/build/client /usr/bin/client

EXPOSE 8082 8082
ENTRYPOINT ["/usr/bin/client"]
